package ru.citeck.ecos.notifications.lib.spring

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["ru.citeck.ecos.notifications.lib.spring"])
open class NotificationsAutoConfiguration
